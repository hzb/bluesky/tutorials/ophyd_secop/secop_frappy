
FROM python:3.11



RUN git clone https://github.com/SampleEnvironment/frappy.git

WORKDIR /frappy

RUN  git checkout frappy-docker-demo


# Install dependencies:
RUN pip install -r requirements.txt


CMD ["python3", "bin/frappy-server","-c","cfg/cryo_cfg.py", "cryo"]
