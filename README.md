## SECoP demo

This repository contains a Dockerfile which can be used to make a simulation of a cryostat with a SECoP interface. 

To start the simulation run `docker-compose up`

